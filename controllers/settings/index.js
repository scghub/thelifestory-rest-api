var express = require('express');
var router = express.Router();
const res = require('../../config/response')
const jwt = require("jsonwebtoken");

const auth = require("../../middleware/auth");
// rethinkdb
const r = require('rethinkdb');
var databaseName = process.env.RDB_DATABASE;
var tableName = "settings"; // set table name


router.get('/',auth, (request,response ) => {

    r.db(databaseName).table(tableName)
        .run(request._rdb)
        .then(cursor => cursor.toArray())
        .then(result => {
            res.data = result
            response.json(res);
        })
        .catch( error => {
            console.log(error)
            res.success = false
            response.json(res);

        });
});

router.get('/token', (request,response ) => {
    const user = {token:null}
    const token = jwt.sign(
        { token_id:  'grzes33@gmail.com'},
        process.env.TOKEN_KEY,
        {
          expiresIn: "3h",
        }
      );
      user.token = token;
      response.json(user);
});


module.exports = router;